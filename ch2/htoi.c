#include <stdio.h>

int htoi(char[]);

int main() {

	printf("%d\n", htoi("0xff"));
	return 0;
}

int htoi(char s[]) {
	int result = 0;
	for (int i = 0; s[i] != '\0'; ++i) {
		if (s[i] >= '0' && s[i] <= '9')
			result = result * 16 + (s[i] - '0');
		else if (s[i] >= 'a' && s[i] <= 'f')
			result = result * 16 + (s[i] - 'a' + 10);
		else if (s[i] >= 'A' && s[i] <= 'F')
			result = result * 16 + (s[i] - 'A' + 10);
		else if (i == 1 && s[i] == 'x' && s[0] == '0')
			result = 0;
		else
			return -1;
	}
	return result;
}