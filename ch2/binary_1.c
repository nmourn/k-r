#include <stdio.h>

#define SIZE 1000000

int bsearch(int x, int a[], int size);
int bsearch2(int x, int a[], int size);

int main() {

	int a[SIZE];
	int dumm;

	for (int i = 0; i < SIZE; ++i)
		a[i] = i;

	for (int i = 0; i < SIZE; ++i)
		for (int j = 0; j < 5; ++j)
			dumm = bsearch(1, a, SIZE);
}

int bsearch(int x, int a[], int size) {

	int low, hi, mid;
	low = 0;
	hi = size-1;
	while (low <= hi) {

		mid = (low+hi)/2;
		if (x > a[mid])
			low = mid+1;
		else if (x < a[mid])
			hi = mid-1;
		else
			return mid;
	}
	return -1;
}

int bsearch2(int x, int a[], int size) {

	int low, hi, mid;
	low = 0;
	hi = size-1;
	mid = (low+hi)/2;
	while (low <= hi && a[mid] != x) {

		if (x < a[mid]) 
			hi = mid - 1;
		else
			low = mid + 1;

		mid = (low+hi) / 2;

	}

	if (x == a[mid])
		return mid;
	else
		return -1;
}