#include <stdio.h>

unsigned getbits(unsigned x, int p, int n);
unsigned setbits(unsigned x, int p, int n, unsigned y);
unsigned invert(unsigned x, int p, int n);
unsigned rightrot(unsigned x, int n);

int main() {

	unsigned x = 0;
	unsigned y = 255;
	x = setbits(x, 5, 1, y);
	printf("%u\n", x);

	x = 0;
	x = invert(x, 1, 2);
	printf("%u\n", x);

	x = 1;
	x = rightrot(x, sizeof(x)*8);
	printf("%u\n", x);
	return 0;
}

/* Get n bits starting from position p */
unsigned getbits(unsigned x, int p, int n) {

	return (x >> (p+1-n) & ~(~0 << n));
}

/* Set n bits of x, starting form position p to first n bits of y */
unsigned setbits(unsigned x, int p, int n, unsigned y) {

	//get mask of everything 0 except n bits starting from p
	unsigned mask = ~(~0 << n) << p;
	// set to 0 n bits starting from p of x
	x = x & ~mask;
	// get first n bits of y
	y = y & ~(~0 << n);
	// move them to pth position
	y = y << p;
	// set bits of y to x
	x = x | y;
	return x;
}

/* Invert n bits of x, starting form position p */
unsigned invert(unsigned x, int p, int n) {

	// get mask of everything 0 except n bits starting from p
	unsigned mask = ~(~0 << n) << p;
	// get target range inverted
	unsigned target = ~x & mask;
	//flush target bits at x
	x = x & ~mask;
	// set inverted target bits back to x
	x = x | target;
	return x;
}

/* Return x, rotated n bits to the right */
unsigned rightrot(unsigned x, int n) {

	//get right part that should be wrapped to left
	unsigned right = x & ~(~0<<n);
	//move right part to the very left
	right = right<<(sizeof(x)*8 - n);
	//move x to the right, throwing away right part and setting left part to 0
	x = x>>n;
	//set right part to x on the left position
	x = x | right;
	return x;
}