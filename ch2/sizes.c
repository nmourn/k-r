#include <stdio.h>
#include <limits.h>

int main() {

	printf("INT_MIN %d\n", INT_MIN);
	printf("INT_MAX %d\n", INT_MAX);
	printf("UINT_MAX %u\n", UINT_MAX);
	printf("LONG_MAX %ld\n", LONG_MAX);
	printf("LONG_MIN %ld\n", LONG_MIN);
	printf("ULONG_MAX %lu\n", ULONG_MAX);

	
}