#include <stdio.h>

void squeeze(char s[], char r[]);
int any(char s1[], char s2[]);

int main() {

	char test[] = "lolka";
	char r[] = "ak";
	printf("%d\n", any(test, "suda"));
	printf("%d\n", any(test, "ugh"));
	printf("%d\n", any(test, "lol"));
	squeeze(test, r);
	printf("%s\n", test);
	return 0;
}

/*
Replace from s all occurences of any char in r
*/
void squeeze(char s[], char r[]) {

	int i, j;
	for (i = j = 0; s[i] != '\0'; i++) {
		int k = 0, match = 0;
		while (r[k] != '\0') {
			if (r[k++] == s[i])
				match = 1;
		}
		if (!match)
			s[j++] = s[i];
	}
	s[j] = '\0';
}

/*
Return first occurence in s1 of any character in s2
-1 if no occurences
*/

int any(char s1[], char s2[]) {
	for (int i = 0; s1[i] != '\0'; i++) {
		int k = 0;
		while (s2[k] != '\0') {
			if (s2[k++] == s1[i])
				return i;
		}
	}
	return -1;
}