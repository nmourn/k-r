/* Prints histogram of lengths of words in the input */
#include <stdio.h>
#include <math.h>

#define IN 1
#define OUT 0
#define MAX_LEN 10

void print_row(char* title, float percentage);
float get_percentage(int current, int total);

main() {

	int c, state, freqslarge, current_len;
	int freqs[MAX_LEN];
	freqslarge = current_len = 0;
	state = OUT;
	for (int i = 0; i < MAX_LEN; ++i)
		freqs[i] = 0;

	while((c = getchar()) != EOF) {

		// if in word and char is alphabet - in word length
		if (c != '\n' && c != ' ' && c != '\t') {
			state = IN;
			++current_len;
		}
		else {
			state = OUT;
			if (current_len > MAX_LEN+1)
				++freqslarge;
			else if (current_len > 0)
				++freqs[current_len-1];
			current_len = 0;
		}
	}
	float total;
	total = freqslarge;
	for (int i = 0; i < MAX_LEN; ++i)
		total += freqs[i];
	printf("percentage:\n");
	for (int i = 0; i < MAX_LEN; ++i) {
		char title[10];
		sprintf(title, "%3d", i+1);
		print_row(title, get_percentage(freqs[i], total));
	}
	char title[10];
	sprintf(title, "%2d+", MAX_LEN+1);
	print_row(title, get_percentage(freqslarge, total));

}

void print_row(char* title, float percentage) {
	printf("%s: %5.1f%% ", title, percentage);
	for (int j = 0; j < percentage/2; ++j)
		printf("=");
	printf("\n");
}

float get_percentage(int current, int total) {
	return floor((float)current/total * 100);
}