#include <stdio.h>

#define IN_WORD 1
#define OUT_WORD 0

void entab_space(int space_count, int col_count, int tab_stops);

int main() {

	int c, state, space_count, col_count, tab_stops;
	space_count = col_count = 0;
	tab_stops = 4;
	state = OUT_WORD;

	while ((c = getchar()) != EOF) {

		// if newline - get out of word and set column count to 0 and print newline
		if (c == '\n') {
			state = OUT_WORD;
			col_count = space_count = 0;
			putchar(c);
		}

		// if space or tab - get out of word, and:
		// for each space increase count of spaces
		// for each tab count how many spaces it should contain and add to total spaces
		else if (c == ' ' || c == '\t') {
			state = OUT_WORD;
			if (c == ' ') {
				++col_count;
				++space_count;
			}
			else {
				int padding = tab_stops - (col_count % tab_stops);
				col_count += padding;
				space_count += padding;
			}
		}
		// if some char and not in word - get in word, add one to col count and print char
		else if (state == IN_WORD) {
			++col_count;
			putchar(c);
		}

		// if some char and not in the word - get in the word and:
		// print as many tabs as possible with respect to number of spaces and total columns
		// all that remains print as spaces
		else {
			state = IN_WORD;
			entab_space(space_count, col_count, tab_stops);
			space_count = 0;
			++col_count;
			putchar(c);
		}

	}

	return 0;
}

void entab_space(int space_count, int col_count, int tab_stops) {

	while (space_count > 0) {

		// how many spaces would tab represent if we put it now
		int padding = tab_stops - ((col_count - space_count) % tab_stops);
		// printf("%d", padding);
		if (padding <= space_count) {
			putchar('\t');
			// printf("\\t(%d)", padding);
			space_count -= padding;
			col_count += padding;
		}
		else {
			for (int i = 0; i < space_count; ++i) {
				putchar(' ');
				// printf("\\s(%d)", space_count);
				--space_count;
				++col_count;
			}
		}
	}
}















