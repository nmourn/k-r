#include <stdio.h>

#define BUFFER_SIZE 1000

void print_seq(char buff[], int start, int end);

int main() {

	int c, last_char, last_space, line_width;
	char buffer[BUFFER_SIZE];
	last_char = 0;
	last_space = -1;
	line_width = 5;

	while ((c = getchar()) != EOF) {

		buffer[last_char] = c;
		if (c == ' ' || c == '\t')
			last_space = last_char;
		last_char++;
		if (last_char == line_width) {
			if (last_space == -1) {
				print_seq(buffer, 0, last_char);
				putchar('\n');
				last_char = 0;
				last_space = -1;
			}
			else {
				print_seq(buffer, 0, last_space);
				putchar('\n');
				last_space++;
				for (int i = last_space; i < last_char; ++i)
					buffer[i-last_space] = buffer[i];
				last_char = last_char-last_space;
				last_space = -1;
			}
		}
		else if (c == '\n') {
			print_seq(buffer, 0, last_char);
			last_char = 0;
			last_space = -1;
		}
	}
	print_seq(buffer, 0, last_char);
}


void print_seq(char buff[], int start, int end) {

	for (int i = start; i < end; ++i)
		putchar(buff[i]);
}