#include <stdio.h>

#define MAX_LEN 1000

int get_line(char line[], int limit);
void copy(char from[], char to[]);
void reverse(char s[]);

int main() {

	int len; // current line length
	int max; // max line length
	char line[MAX_LEN];
	char max_line[MAX_LEN];

	while ((len = get_line(line, MAX_LEN)) > 0) {

		if (len > max) {
			max = len;
			copy(line, max_line);
		}
	}
	reverse(max_line);
	if (max > 0)
		printf("%s", max_line);

	return 0;
}

int get_line(char line[], int limit) {

	int c, i;

	for (i = 0; i < limit-1 && ((c = getchar()) != EOF) && c != '\n'; ++i)
		line[i] = c;

	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}
void copy(char from[], char to[]) {

	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0') {
		++i;
	}
}

void reverse(char s[]) {
	int len;
	len = 0;
	while (s[len] != '\0') ++len;
	char rev[len];
	for (int i = 1; i < len; ++i)
		rev[len-i-1] = s[i-1];
	rev[len-1] = '\0';
	copy(rev, s);
}