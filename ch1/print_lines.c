#include <stdio.h>

#define IN 1
#define OUT 0

main() {

	int c, state;

	state = OUT;

	while ((c = getchar()) != EOF) {
		if (state == IN && (c == ' ' || c == '\t' || c == '\n')) {
			putchar('\n');
			state = OUT;
		}
		else if (c != '\n' && c != '\t' && c != ' ') {
			putchar(c);
			state = IN;
		}
	}
}