#include <stdio.h>

int write_tab(int len, int stops_num);

int main() {

	int c, l;
	int stops_num = 4;
	l = 0;

	while ((c = getchar()) != EOF) {

		if (c == '\t') {
			int space_num = write_tab(l, stops_num);
			l += space_num;
		}
		else {
			putchar(c);
			++l;
		}
		if (c == '\n')
			l = 0;

	}

	return 0;
}

// write tabulation and return number of spaces printed
int write_tab(int len, int stops_num) {

	int space_num = stops_num - len % stops_num;
	for (int i; i < space_num; ++i)
		putchar(' ');
	return space_num;
}