#include <stdio.h>

#define LOWER 0 
#define UPPER 300
#define STEP 10


main() {

	float fahr, celsius, fahr2;

	printf("%s\t%s\t%s\n", "Fahr", "Celsius", "Fahr2");
	for (fahr = UPPER; fahr >= LOWER; fahr -= STEP) {
		celsius = (5.0/9.0) * (fahr-32);
		fahr2 = (9.0/5.0 * celsius) + 32;
		printf("%3.0f\t%6.2f\t%3.0f\n", fahr, celsius, fahr2);
	}
}
