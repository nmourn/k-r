#include <stdio.h>

main() {

	int c;

	while ((c = getchar()) != EOF) {
		if (c == ' ')
			putchar(c);
		while (c == ' ' && c != EOF)
			c = getchar();
		putchar(c);
	}
}