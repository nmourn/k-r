#include <stdio.h>

main() {

	int c;
	long count = 0;
	while ((c = getchar()) != EOF) {
		if (c == '\n' || c == ' ' || c == '\t')
			++count;
	}
	printf("Empties count: %ld\n", count);
}